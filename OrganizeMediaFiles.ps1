# ==============================================================================================
# 
# Microsoft PowerShell Source File 
# 
# This script will organize photo and video files by renaming the file based on the date the
# file was created and moving them into folders based on the year and month. It will also append
# a random number to the end of the file name just to avoid name collisions. The script will
# look in the SourceRootPath (recursing through all subdirectories) for any files matching
# the extensions in FileTypesToOrganize. It will rename the files and move them to folders under
# DestinationRootPath, e.g. DestinationRootPath\2011\02_February\2011-02-09_21-41-47_680.jpg
# 
# JPG files contain EXIF data which has a DateTaken value. Other media files have a MediaCreated
# date. 
#
# The code for extracting the EXIF DateTaken is based on a script by Kim Oppalfens:
# http://blogcastrepository.com/blogs/kim_oppalfenss_systems_management_ideas/archive/2007/12/02/organize-your-digital-photos-into-folders-using-powershell-and-exif-data.aspx
# ============================================================================================== 

param
(
    [string]$SourceRootPath = ".\RecentUploads",
    [string]$DestinationRootPath = ".",
    [string]$operation = "Copy",
    [string]$photosOnly = "false"
)

if ($operation -ne "Copy" -and $operation -ne "Move")
{
    Write-Output "Operation parameter must be either `"Copy`" or `"Move`""
    Exit
}

[reflection.assembly]::loadfile( "C:\Windows\Microsoft.NET\Framework\v2.0.50727\System.Drawing.dll") | Out-Null

if ($photosOnly -eq "true")
{
    $FileTypesToOrganize = @("*.jpg")
}
else
{
    $FileTypesToOrganize = @("*.jpg","*.avi","*.mp4")
}

function GetMediaCreatedDate($File) {
	$Shell = New-Object -ComObject Shell.Application
	$Folder = $Shell.Namespace($File.DirectoryName)
	$CreatedDate = $Folder.GetDetailsOf($Folder.Parsename($File.Name), 191).Replace([char]8206, ' ').Replace([char]8207, ' ')

	if (($CreatedDate -as [DateTime]) -ne $null) {
		return [DateTime]::Parse($CreatedDate)
	} else {
		return $null
	}
}

function ConvertAsciiArrayToString($CharArray) {
	$ReturnVal = ""
	foreach ($Char in $CharArray) {
		$ReturnVal += [char]$Char
	}
	return $ReturnVal
}

function GetExifDateTaken($File) {
	$FileDetail = New-Object -TypeName System.Drawing.Bitmap -ArgumentList $File.Fullname 
	$DateTimePropertyItem = $FileDetail.GetPropertyItem(36867)
	$FileDetail.Dispose()

    if ($DateTimePropertyItem -eq $null)
    {
        return $null
    }
    
	$Year = ConvertAsciiArrayToString $DateTimePropertyItem.value[0..3]
	$Month = ConvertAsciiArrayToString $DateTimePropertyItem.value[5..6]
	$Day = ConvertAsciiArrayToString $DateTimePropertyItem.value[8..9]
	$Hour = ConvertAsciiArrayToString $DateTimePropertyItem.value[11..12]
	$Minute = ConvertAsciiArrayToString $DateTimePropertyItem.value[14..15]
	$Second = ConvertAsciiArrayToString $DateTimePropertyItem.value[17..18]
	
	$DateString = [String]::Format("{0}/{1}/{2} {3}:{4}:{5}", $Year, $Month, $Day, $Hour, $Minute, $Second)
	
	if (($DateString -as [DateTime]) -ne $null) {
		return [DateTime]::Parse($DateString)
	} else {
		return $null
	}
}

function GetCreationDate($File) {
    try
    {
    	switch ($File.Extension) { 
            ".jpg" { $CreationDate = GetExifDateTaken($File) } 
            default { $CreationDate = GetMediaCreatedDate($File) }
        }
    }
    catch
    {
        $CreationDate = $null
    }
    
	return $CreationDate
}

function BuildDesinationPath($Path, $Date) {
	return [String]::Format("{0}\{1}\{2}_{3}", $Path, $Date.Year, $Date.ToString("MM"), $Date.ToString("MMMM"))
}

$RandomGenerator = New-Object System.Random
function BuildNewFilePath($Path, $Date, $Extension, [boolean]$AddRandomSuffix) {
    if ($AddRandomSuffix)
    {
        return [String]::Format("{0}\{1}_{2}{3}", $Path, $Date.ToString("yyyy-MM-dd_HHmmss"), $RandomGenerator.Next(100, 1000).ToString(), $Extension)
    }
    else
    {
        return [String]::Format("{0}\{1}{2}", $Path, $Date.ToString("yyyy-MM-dd_HHmmss"), $Extension)
    }
}

function CreateDirectory($Path){
	if (!(Test-Path $Path)) {
		New-Item $Path -Type Directory > $null
	}
}

function ConfirmContinueProcessing() {
	$Response = Read-Host "Continue? (Y/N)"
	if ($Response.Substring(0,1).ToUpper() -ne "Y") { 
		break 
	}
}

# set this now in case it's needed
$undatedDirectory = "$DestinationRootPath\ByCreatedDate"

Write-Host "Begin - operation is $operation"
$Files = Get-ChildItem $SourceRootPath -Recurse -Include $FileTypesToOrganize
foreach ($File in $Files)
{
	$CreationDate = GetCreationDate($File)
    
    if ($CreationDate -eq $null)
    {
        $CreationDate = (Get-Item $File).CreationTime # this is the file created time from Windows
        Write-Host "Used creation date of $CreationDate"
    }
    
	$DestinationPath = BuildDesinationPath $DestinationRootPath $CreationDate
	CreateDirectory $DestinationPath
	$NewFilePath = BuildNewFilePath $DestinationPath $CreationDate $File.Extension $false
		
    if (Test-Path $NewFilePath)
    {        
        # generate checksums for source and destination file
        $md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
        $stream = [System.IO.File]::Open($File.FullName, [System.IO.Filemode]::Open, [System.IO.FileAccess]::Read)
        $sourceFileHash = [System.BitConverter]::ToString($md5.ComputeHash($stream))
        $stream.Close()
        $stream = [System.IO.File]::Open($File.FullName, [System.IO.Filemode]::Open, [System.IO.FileAccess]::Read)
        $destFileHash = [System.BitConverter]::ToString($md5.ComputeHash($stream))
        $stream.Close()

        if ($sourceFileHash -ne $destFileHash)
        {
            # the files are not the same
        
            # build a file name with the hash code added
            $filePathWithHash = Split-Path $NewFilePath
            if ($filePathWithHash -eq "")
            {
                $filePathWithHash = "."
            }
            $filePathWithHash += "\" + [System.IO.Path]::GetFileNameWithoutExtension($NewFilePath) + "_" + $sourceFileHash.Replace("-", "")
            $filePathWithHash += [System.IO.Path]::GetExtension($NewFilePath)
            
            if (Test-Path $filePathWithHash)
            {
                # do nothing because the file with hash already exists and is a true duplicate
                $NewFilePath = $null
            }
            else
            {
                # use the file name with hash as the new file name
                $NewFilePath = $filePathWithHash
            }
        }
        else
        {
            # do nothing because the file with hash already exists and is a true duplicate
            $NewFilePath = $null
        }
	}
    
    if ($NewFilePath -ne $null)
    {
        #Write-Host "Putting $($File.Name) in $NewFilePath"
        if ($operation -eq "Move")
        {
    	    Move-Item $File.FullName $NewFilePath
        }
        elseif ($operation -eq "Copy")
        {
            Copy-Item $File.FullName $NewFilePath
        }
    }
} 

Write-Host "Done"
